package server;

import java.rmi.Remote;
import java.rmi.RemoteException;

import client.ClientIF;

public interface RegistryIF extends Remote{
	
	public boolean register(String uname, String paswd) throws RemoteException;
	
	public int login(String uname, String paswd, ClientIF callback) throws RemoteException;
	
	public boolean logout(String uname) throws RemoteException;
	
	public boolean quitgame(String uname, String master) throws RemoteException;
	
	public void gameover(String master) throws RemoteException;
	
}
