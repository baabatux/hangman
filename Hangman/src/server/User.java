package server;

import client.ClientIF;

public class User {
	private ClientIF callback;
	private String username;
	private String password;
	
	public User(String username, String password){
		this.username = username;
		this.password = password;
		this.callback = null;
	}
	
	public String getUsername(){
		return username;
	}
	
	public boolean checkPwd(String pwd){
		return (password.equals(pwd));
	}
	
	public void setOnline(ClientIF callback){
		this.callback = callback;
	}
	
	public ClientIF getCallback(){
		return callback;
	}
	
	public void setOffline(){
		this.callback = null;
	}
	
	@Override
	public String toString(){
		return String.format("[u:%s p:%s c:%s]", username, password, callback);
	}
}
