package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class CreateJoinHandler extends Thread{
	private DataStruct utentipartite;
	private Socket connection;
	private BufferedReader in;
	private PrintWriter out;
	private String uname;
	private int CREATETIMEOUT;
	private int GAMETIMEOUT;
	
	public CreateJoinHandler(Socket connection, DataStruct utentipartite, int createTO, int gameTO){ 
		this.connection = connection;
		this.utentipartite = utentipartite;
		this.CREATETIMEOUT = createTO;
		this.GAMETIMEOUT = gameTO;
	}

	@SuppressWarnings("unchecked")
	public void run(){
		JSONObject sndobj = new JSONObject();
		JSONParser pars = new JSONParser();
		JSONObject rcvobj = null;
		String comando = null;
		
		try{
			in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			out = new PrintWriter(connection.getOutputStream(), true);
			
			rcvobj = (JSONObject) pars.parse(in.readLine());
			uname = (String) rcvobj.get("name");
			comando = (String) rcvobj.get("richiesta");
			if (uname == null || comando == null) throw new ParseException(1);
		} catch (IOException | ParseException | NullPointerException e){ //null se si chiude la socket o il dato è sbagliato
			rcvobj = null; //a null per non farlo processare
		}
		
		if ( comando.equals("master") && rcvobj != null){
				int Nguesser = new Integer(rcvobj.get("Nguesser").toString());
				if (utentipartite.newgame(uname, connection, Nguesser)) {
					sndobj.put("esito", "OK");
					out.println(sndobj.toJSONString());
					try {
						//rimane attivo per partita in costruzione aspettando "quit" o "start"
						connection.setSoTimeout(CREATETIMEOUT);
						rcvobj = (JSONObject) pars.parse(in.readLine());
						
						comando = (String) rcvobj.get("richiesta");
						if (comando.equals("quit")){
							utentipartite.abortgame(uname);
						} else if (comando.equals("start")){
							//partita cominciata
						}
					} catch (ParseException | IOException | NullPointerException e){ //cattura anche SocketTimeoutException
						utentipartite.abortgame(uname);
					}
				}
				else {
					//comunicazione errore
					sndobj.put("esito", "Impossibile creare partita");
					out.println(sndobj.toJSONString());
				}
				
			} else if (comando.equals("guesser") && rcvobj != null){
				String mastername = (String) rcvobj.get("mastername"); 
				int esito = utentipartite.joingame(uname, mastername, connection, this);
				if (esito < 0) {
					//partita abortita
					sndobj.put("esito", "Impossibile entrare nella partita");
					out.println(sndobj.toJSONString());
				}
				else if (esito == 0) { 
					//la partita è stata avviata, questo thread controlla il timeout
					//resta attivo e viene interrrotto se la partita viene annullata
					//(con RMI il master comunica la fine della partita o l'interruzione)					
					try { 
						Thread.sleep(GAMETIMEOUT);
						utentipartite.gameover(mastername);
					} catch (InterruptedException e) { 
						//la partita è finita ed è stato aggiornato 
					}
				} 
				else {
					//si è entrati nella partita e si aspetta l'inizio o il quit
					try{
						rcvobj = (JSONObject) pars.parse(in.readLine());
						comando = (String) rcvobj.get("richiesta");
						if (comando.equals("quit")) utentipartite.leavegame(uname, mastername);
						else
							if (comando.equals("start")); //termina thread
							else { 
								System.err.printf("Errore protocollo connessione con %s%n", uname);
								utentipartite.leavegame(uname,mastername);
								utentipartite.logout(uname);
								sndobj.put("esito", "abort");
						}
					} catch (ParseException | IOException e){
						System.err.printf("Errore protocollo connessione con %s : %s%n ", uname, e);
						utentipartite.leavegame(uname, mastername);
						utentipartite.logout(uname);
						sndobj.put("esito", "abort");
					} catch (NullPointerException e){
						utentipartite.leavegame(uname, mastername);
						//utentipartite.logout(uname);
						sndobj.put("esito", "abort");
					}
				}	
			}
			else {
				System.err.printf("Errore protocollo connessione con %s%n", uname);
				utentipartite.logout(uname);
			}
	 
		try {connection.close();
		} catch (IOException e) {System.err.printf("Errore protocollo connessione con %s : %s%n", uname, e);}
		
	}
}
