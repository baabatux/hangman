package server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import client.ClientIF;

public class RegistryRMI extends UnicastRemoteObject implements RegistryIF {

	private static final long serialVersionUID = 1L;
	private DataStruct db;

	public RegistryRMI(DataStruct db) throws RemoteException {
		this.db = db;
	}

	public boolean register(String uname, String paswd) throws RemoteException {
		return db.register(uname, paswd);
	}

	public int login(String uname, String paswd, ClientIF callback) throws RemoteException {
		return db.login(uname, paswd, callback);
	}

	public boolean logout(String uname) throws RemoteException {
		return db.logout(uname);
	}

	public boolean quitgame(String uname, String master) throws RemoteException{
		if( uname == master) {
			db.gameover(master);
			return true;
		}
		else return db.leavegame(uname, master);
	}

	public void gameover(String master) throws RemoteException {
		db.gameover(master);
	}

}
