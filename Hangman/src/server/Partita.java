package server;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.LinkedList;
import java.util.List;

import org.json.simple.JSONObject;
import java.util.Random;

public class Partita {
	
	private Gruppo grup;
	private String masterName;
	private Socket masterSoc;
	private UserSocket[] guessers;
	private int GSpointer; //alla prima libera
	private PrintWriter out;
	private JSONObject sndobj;
	private Random random;
	private Thread timeoutThread;
	private int gametimeout;
		
	public Partita(Gruppo grup, int gametimeout) {
		this.grup = grup;
		this.sndobj = new JSONObject();
		this.masterName = null;
		this.masterSoc = null;
		this.guessers = null;
		this.GSpointer = -1;
		this.random = new Random();
		this.gametimeout = gametimeout;
	}
	
	public void init(String master, Socket msoc, int n){
		this.masterName = master;
		this.masterSoc = msoc;
		this.guessers = new UserSocket[n];
		this.GSpointer = 0;
	}
		
	public int addGuesser(String guesser, Socket soc, Thread timeoutThread){
			if (GSpointer < guessers.length){
				guessers[GSpointer] = new UserSocket(guesser, soc);
				GSpointer++;
				if (GSpointer == guessers.length) {
					notificaTCPstart();
					masterSoc = null;
					this.timeoutThread = timeoutThread;
				} else notificaTCPjoin();
				return guessers.length - GSpointer;
			}
			else return -1;
	}
	
	public boolean removeGuesser(String guesser){
		boolean flag = false;
		int i = 0;
		if (GSpointer > 0){
			while(i < GSpointer && flag == false){
				if (guessers[i].getName() == guesser){
					guessers[i] = guessers[GSpointer - 1];
					GSpointer--;
					flag = true; 
				}
				else i++;
			}
			if (masterSoc != null && flag) notificaTCPjoin(); //se la partita non è iniziata ed è cambiato qualcosa
			return flag;
		}else return false;	
	}
	
	public LinkedList<String> abort(){
		notificaTCPabort();
		return this.gameover();
	}
	
	public LinkedList<String> gameover(){
		if (timeoutThread != null) timeoutThread.interrupt();
		LinkedList<String> utenti = new LinkedList<String>();
		utenti.add(masterName);
		for(int i=0; i<GSpointer; i++){
			utenti.add(this.guessers[i].getName());
		}
		return utenti;
	}

	public Gruppo getGroup() {
		return grup;
	}

	
	public String toString(){
		return String.format("/IP: %s Master: %s Guessers: %s NumeroGues: %s MaxGues: %s /",
				grup.getAddress().getHostAddress(), masterName, guessertoNameList(), GSpointer, guessers.length);
	}

	@SuppressWarnings("unchecked")
	private void notificaTCPjoin(){
		sndobj.clear();
		sndobj.put("esito", "join");
		sndobj.put("list", guessertoNameList());
		sndobj.put("mancanti", guessers.length - GSpointer);
		notificaTCP(sndobj);
	}

	@SuppressWarnings("unchecked")
	private void notificaTCPstart(){
		sndobj.clear();
		sndobj.put("esito", "start");
		sndobj.put("list", guessertoNameList());
		sndobj.put("gruppo", grup.toJSON());
		sndobj.put("timeout", gametimeout);
		String chiave = String.format("%d%d",random.nextInt(), guessers.length);
		sndobj.put("chiave", chiave);
		notificaTCP(sndobj);
	}
	
	@SuppressWarnings("unchecked")
	private void notificaTCPabort(){
		sndobj.clear();
		sndobj.put("esito", "abort");
		notificaTCP(sndobj);
	}
	
	private void notificaTCP(JSONObject sndobj){
		try {
			out = new PrintWriter(masterSoc.getOutputStream(), true);
			out.println(sndobj.toJSONString());
		} catch (IOException e) {}//TODO ?
			
		
		for(int i=0; i<GSpointer; i++){
			try {	
				out = new PrintWriter(this.guessers[i].getSocket().getOutputStream(), true);
				out.println(sndobj.toJSONString());
			} catch (IOException e) {}//TODO ?
		}
	}
	

	
	private List<String> guessertoNameList(){
		List<String> l= new LinkedList<String>();
		
		for(int i=0; i < GSpointer; i++) 
			l.add(guessers[i].getName());
		return l;
	}

	@SuppressWarnings("unchecked")
	public JSONObject toJSON() {
		JSONObject o = new JSONObject();
		o.put("master", masterName);
		o.put("guesserMax", guessers.length);
		o.put("guesserTot", GSpointer);
		return o;
	}
		
	
}


class UserSocket{
	private String username;
	private Socket socket;
	
	public UserSocket(String guessername, Socket socket){
		this.username = guessername;
		this.socket = socket;
	}

	public String getName() {
		return username;
	}

	public Socket getSocket() {
		return socket;
	}
	
}