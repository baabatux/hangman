package server;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;

import org.json.simple.JSONObject;

public class Gruppo {
	
	private InetAddress addr;
	private int masterport;
	private int guesserport;
	
	
	public Gruppo(String ip, int masterport, int guesserport) throws UnknownHostException{
		this.addr = Inet4Address.getByName(ip);
		this.masterport = masterport;
		this.guesserport = guesserport;
	}
	
	public Gruppo(JSONObject obj) throws UnknownHostException{
		addr = Inet4Address.getByName((String) obj.get("ip"));
		masterport = new Integer(obj.get("masterport").toString());
		guesserport = new Integer(obj.get("guesserport").toString());
		
	}
	
	public InetAddress getAddress() { 
		return addr;
		}
	
	public int getMasterport() { 
		return masterport;
		}
	
	public int getGuesserport() { 
		return guesserport;
		}
	
	@SuppressWarnings("unchecked")
	public JSONObject toJSON(){
		JSONObject obj = new JSONObject();
		obj.put("ip", addr.getHostName());
		obj.put("masterport", masterport);
		obj.put("guesserport", guesserport);
		return obj;
	}

}
