package server;

import java.net.Socket;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

import client.ClientIF;
// si potrebbe dire qualcosa sul fatto che registrazione e login vanno a bloccare la creazone di partite
// il punto è che per il login e logout devo andare a controllare che non siano in gioco e questo
// coninvolge l'uso della struttura INGAME che deve essere mantenuta coerente con PARTITE
public class DataStruct {
	private HashMap<String, User> regist;
	private HashMap<String,User> lobby;
	private HashMap<String,User> ingame;
	private PartiteStruct partite;
	
	public DataStruct(LinkedList<Gruppo> gruppi, int maxGame, int gametimeout){
		regist = new HashMap<String, User>();
		lobby = new HashMap<String,User>();
		ingame = new HashMap<String,User>();
		partite = new PartiteStruct(gruppi, maxGame, gametimeout);
	}
	
	public synchronized boolean register(String uname, String paswd){
		if (regist.containsKey(uname)) return false;
		else{
			User user = new User(uname, paswd);
			regist.put(uname, user);
			return true;
		}
	}
	
	public synchronized int login(String uname, String paswd, ClientIF callback){
		User user = regist.get(uname);
		if (user != null && callback != null) 
			if (!lobby.containsKey(uname) && !ingame.containsKey(uname)) 
				if (user.checkPwd(paswd)) {
					lobby.put(uname, user); //lo copio
					user.setOnline(callback);
					try {
						callback.notifica(partite.toJSONnotify());
					} catch (RemoteException e) {
						System.err.println("Errore login " + uname + ":" + e);
						lobby.remove(uname);
						user.setOffline();
						return -3;
					}
					return 0; //connesso
				} else return -1; //psw sbagliata
			else return -2; //già online
		else return -3; // non esiste/callback errata
				
	}
	
	public synchronized boolean logout(String uname){
		User user = lobby.remove(uname);
		if (user != null){
				user.setOffline();
				return true; //disconnesso
			} else return false; // non è nella lobby ( in gioco o già offline )
	}
	
	public synchronized boolean newgame(String uname, Socket msoc, int Nguesser){
		User user = lobby.get(uname);
		if(user != null && Nguesser > 0){ 
			if (partite.newGame(uname, msoc, Nguesser)){
				lobby.remove(uname);
				// per efficienza del controllo login e unicità partite uso l'hashmap ingame
				ingame.put(uname, user);
				notificaAllRMI();
				return true;
			} else return false;
		} else return false;
	}

	public synchronized int joingame(String uname, String mastername, Socket gsoc, Thread timeoutThread){
		int esito = partite.newGuesser(uname, mastername, gsoc, timeoutThread);
		if (esito >= 0) {
			User user = lobby.remove(uname);
			ingame.put(uname, user);
			notificaAllRMI();
			return esito;
		} else return -1;
	}
	
	public synchronized boolean leavegame(String guesser, String master){
		if (partite.removeGuesser(guesser, master)){
			User user = ingame.remove(guesser);
			lobby.put(guesser, user);
			notificaAllRMI();
			return true;
		} else return false;
	}
	
	public synchronized void abortgame(String master){
		LinkedList<String> utenti = partite.abortGame(master);
		Iterator<String> iter = utenti.iterator();
		User user;
		String name;
		while(iter.hasNext()){
			name = iter.next();
			user = ingame.remove(name);
			lobby.put(name, user);
		}
		notificaAllRMI();
	}
	
	public synchronized void gameover(String master){
		LinkedList<String> utenti = partite.gameOver(master);
		
		Iterator<String> iter = utenti.iterator();
		User user;
		String name;
		while(iter.hasNext()){
			name = iter.next();
			user = ingame.remove(name);
			lobby.put(name, user);
		}
		notificaAllRMI();
	}
	
	private void notificaAllRMI(){
		
		@SuppressWarnings("unchecked")
		Iterator<User> iter = ((HashMap<String,User>) lobby.clone()).values().iterator();
		User user = null;
		while(iter.hasNext()){
			user = iter.next();
			notificaRMI(user);
		}
	}
	
	private void notificaRMI(User user){
			try {
				user.getCallback().notifica(partite.toJSONnotify());
			} catch (RemoteException e) {
				//aggiorno le tabelle sloggando l'utente
				if (user != null) logout(user.getUsername()); 

			}
		}
	
	public synchronized String dump(){
		return String.format(" utenti: %s%n" +
				" online: %s%n" +
				" ingame: %s%n" +
				" partite: %s%n"
				,regist, lobby, ingame, partite);
	}
	
	
	
}
