package server;

import java.net.Socket;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.json.simple.JSONObject;

public class PartiteStruct {
	private LinkedList<Partita> partiteDisp = new LinkedList<Partita>(); //una coda di partite disponibili!
	private int trueMax;
	private int Nattive;
	private HashMap<String, Partita> partiteAttesa;
	private HashMap<String, Partita> partiteGioco;
	
	
	public PartiteStruct(LinkedList<Gruppo> gruppi, int Nmax, int gametimeout){
		Iterator<Gruppo> iter = gruppi.iterator();
		while(iter.hasNext()){
			partiteDisp.add(new Partita(iter.next(), gametimeout));
			
		}
		this.trueMax = Math.min(Nmax, partiteDisp.size());
		partiteAttesa = new HashMap<String, Partita>();
		partiteGioco = new HashMap<String, Partita>();
	}
	
	public boolean newGame(String master, Socket msoc, int n){
		if (Nattive < trueMax) { //se si può
			Partita p = this.pollGame();
			if (p != null){ //se effettivamente ce ne sono libere
				p.init(master, msoc, n);
				partiteAttesa.put(master, p);
				return true;
			}
			else return false;
		} else return false;
	}
	
	public int newGuesser(String guesser, String master, Socket gsoc, Thread timeoutThread){
		Partita p = partiteAttesa.get(master);
		if (p != null){
			int esito = p.addGuesser(guesser, gsoc, timeoutThread);
			if (esito == 0) {
					partiteAttesa.remove(master);
					partiteGioco.put(master,p);
			}
			return esito;
		}
		else return -1;
	}
	
	public boolean removeGuesser(String guesser, String master){
		Partita p = partiteAttesa.get(master);
		if (p == null) p = partiteGioco.get(master); //se non era in attesa allora la cerco in quelle in gioco
		if (p != null) return p.removeGuesser(guesser);
		else return false;
	}
	
	public LinkedList<String> abortGame(String master){
		Partita p = partiteAttesa.remove(master);
		if (p!=null){
			this.pushGame(p);
			return p.abort();
		}
		else return new LinkedList<String>();
	}
	
	public LinkedList<String> gameOver(String master){
		Partita p = partiteGioco.remove(master);
		if (p!=null){
			this.pushGame(p);
			return p.gameover();
		}
		else return new LinkedList<String>();
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject toJSONnotify(){
		JSONObject o = new JSONObject();
		o.put("disponibili", trueMax - Nattive);
		o.put("massimo", trueMax);
		Iterator<Partita> iter = partiteAttesa.values().iterator();
		List<JSONObject> partitelist = new LinkedList<JSONObject>();
		while(iter.hasNext()){
			partitelist.add(iter.next().toJSON());
		}
		o.put("partite", partitelist);
		return o;
	}
	
	public String toString(){
		return String.format("||partiteDisp: %s Nmax: %s Nattive: %s partiteAttesa: %s partiteGioco: %s ||",
					partiteDisp, trueMax, Nattive, partiteAttesa, partiteGioco);
	}
	
	private Partita pollGame(){
			Nattive++;
			return partiteDisp.poll();
	}
	
	private void pushGame(Partita partita){
			Nattive--;
			partiteDisp.add(partita);
	}
	
	
}

