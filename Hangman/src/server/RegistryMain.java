package server;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.ServerSocket;
import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


public class RegistryMain {
	private static String LOCALIF;
	private static int RMIPORT;
	private static int TCPPORT;
	private static int MAXGAME;
	private static LinkedList<Gruppo> GRPLIST;
	private static int GAMETO;
	private static int CREATETO;
	
	
	public static void main(String[] args){
		ServerSocket soc = null;
		
		System.out.println("Inizializzazione server");
		try {
			init("REG.conf");
		} catch (FileNotFoundException e) {
			System.err.println("Il file non esiste");
			System.exit(1);
		} catch (IOException e) {
			System.err.println("Errore lettura file: " + e);
			System.exit(1);
		} catch (ParseException e) {
			System.err.println("File corrotto");
			System.exit(1);
		}
		System.out.printf(" INTERFACE: %s%n" +     
						" RMIPORT: %s%n" +    
						" TCPPORT: %s%n" +   
						" GRPLISTLEN: %d%n" +  
						" MAXGAME: %s%n" + 
						" GAMETIMEOUT: %s%n" +
						" CREATIONTIMEOUT: %s%n",
						LOCALIF, RMIPORT, TCPPORT, GRPLIST.size(), MAXGAME, GAMETO, CREATETO  
						);
		
		DataStruct database = new DataStruct(GRPLIST, MAXGAME, GAMETO);
		System.setProperty("java.rmi.server.hostname", LOCALIF);
		try {
			RegistryRMI rmiserv = new RegistryRMI(database);
			
			Registry reg = LocateRegistry.createRegistry(RMIPORT);
			reg.bind("hangman",rmiserv);
			
		} catch (RemoteException | AlreadyBoundException e) {
			System.err.println("Impossibile inizializzare server RMI" + e);
			System.exit(1);
		}
		
		try {
			soc = new ServerSocket(TCPPORT);
		} catch (IOException e) {
			System.err.println("Impossibile inizializzare server TCP:" + e);
			System.exit(1);
		}
			
		
		boolean listening = true;
		while(listening){
			try {
				(new CreateJoinHandler(soc.accept(), database, CREATETO, GAMETO)).start();
			} catch (IOException e) {
				//CATTURA ANCHE PARSEEXPTION
				e.printStackTrace();
			}
		}
		
	}

	private static void init(String confpath) throws FileNotFoundException, IOException, ParseException{
		JSONObject confobj = (JSONObject) new JSONParser().parse(new FileReader(confpath));
		TCPPORT = new Integer(confobj.get("tcpport").toString());
		RMIPORT = new Integer(confobj.get("rmiport").toString());
		LOCALIF = (String) confobj.get("localif");
		MAXGAME = new Integer(confobj.get("maxgame").toString());
		GAMETO = new Integer(confobj.get("gameTO").toString());
		CREATETO = new Integer(confobj.get("createTO").toString());
		GRPLIST = new LinkedList<Gruppo>();
		@SuppressWarnings("unchecked")
		List<JSONObject> giplist = (List<JSONObject>) confobj.get("grouplist");		
		Iterator<JSONObject> iter = giplist.iterator();
		while(iter.hasNext()){
			GRPLIST.add(new Gruppo(iter.next()));
		}
	}
	
}
