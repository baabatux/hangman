package client;

import org.json.simple.JSONObject;

public class StatoPartita { //non ha sincronizzazione perchè le lettere arrivate sono gestite una per volta
	private char[] arrc;
	private String parola;
	private int errori, errMax;
	private final char blank = '#';
	
	public StatoPartita(String parola, int errMax) {
		this.parola = parola;
		this.errori = 0;
		this.errMax = errMax;
		arrc = new char[parola.length()];
		for (int i=0; i<arrc.length; i++) arrc[i]=blank;
	}
	
	public int update(String c){
		int blankN = 0;
		char ch = c.charAt(0);
		
		if (!parola.contains(c)) errori++;
		int i;
		for(i=0; i<parola.length(); i++) {
			if(parola.charAt(i) == ch) {
				arrc[i] = ch; 
			}
			else if (arrc[i] == blank) blankN++;
		}	
		if (blankN == 0) return 1;
		if (errori == errMax) {arrc = parola.toCharArray() ; return -1;} 
		return 0;
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject toJson(){
		String s = new String(arrc);
		JSONObject obj = new JSONObject();
  		obj.put("statoparola", s);
  		obj.put("errori", this.errori);
		
  		System.out.print(obj);
		System.out.println();
		return obj;
	}
}
