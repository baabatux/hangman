package client;

public class GuesserStatus {
	private boolean gameOver;
	private String username;
	private boolean waitingACK;
	
	public GuesserStatus(String username){
		this.waitingACK = false;
		this.gameOver = false;
		this.username = username;
	}
	public String getUsername(){
		return username;
	}

	public void waitACK(){
		waitingACK = true;
	}

	public boolean isWaitingACK(){
		return waitingACK;
	}
	
	public void ACKrecieved(){
		waitingACK = false;
	}

	public synchronized void gameOver(){
		gameOver = true;
	}
	
	public synchronized boolean isGameOver(){
		return gameOver;
	}
}
