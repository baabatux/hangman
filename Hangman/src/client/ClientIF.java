package client;

import java.rmi.Remote;
import java.rmi.RemoteException;

import org.json.simple.JSONObject;

public interface ClientIF extends Remote {
	

	public void notifica(JSONObject partitejson) throws RemoteException;
}
