package client;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.concurrent.atomic.AtomicBoolean;

import org.jasypt.util.text.BasicTextEncryptor;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class MasterMulticastHandler extends Thread{
	private MulticastSocket soc;
	private InetAddress group;
	private byte[] bufrcv;
	private DatagramPacket rcvp;
	private JSONObject obj;
	private JSONParser pars;
	private AtomicBoolean gameover;
	private StatoPartita statopar;
	private int guessport;
	private BasicTextEncryptor encr; 
	private boolean quit;
	private TimeOutTH toThread;
	
	public MasterMulticastHandler(AtomicBoolean gameover, MulticastSocket soc, InetAddress group, StatoPartita statopar, int guessport, int timeout, String chiave){
		this.soc = soc;
		this.group = group;
		this.bufrcv = new byte[256];
		this.rcvp = new DatagramPacket(bufrcv, bufrcv.length);
		this.pars = new JSONParser();
		this.statopar = statopar;
		this.guessport = guessport;
		this.gameover = gameover;
		this.encr = new BasicTextEncryptor();
		this.encr.setPassword(chiave);
		this.toThread = new TimeOutTH(this, timeout);
	}
	
	public void run(){
		try {
			toThread.start();
			this.handleMaster(statopar);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void handleMaster(StatoPartita statopar) throws IOException{
		boolean indovinata = false;
		boolean perso = false;
		soc.joinGroup(group);
		
		try{
			while(notQuit() && !indovinata && !perso){
				soc.receive(rcvp);
				String recstr = encr.decrypt(new String(rcvp.getData(), 0, rcvp.getLength()));
				
				try {
					obj = (JSONObject) pars.parse(recstr.toString());
					
					String user = (String) obj.get("user");
					String c = (String) obj.get("lettera");
					switch (statopar.update(c)) {
					case 1 : 	
						toThread.interrupt();
						indovinata = true;
						this.finish("indovinata", statopar);
						gameover.set(true);
						break;
					case -1 :
						toThread.interrupt();
						perso = true; 
						this.finish("impiccato", statopar);
						gameover.set(true);
						break;
					case 0 : this.update(user, statopar);
					}
				} catch (NullPointerException | ParseException e) {}
			}
			
		} catch (IOException e){
			if(notQuit()) throw new IOException();
		}
	}
	
	public synchronized void quit(){
		quit = true;
		this.toThread.interrupt();
		try {
			this.finish("master quit", statopar);
		} catch (IOException e) {}
		soc.close();
	}
	
	public synchronized void timeout(){
		quit = true;
		try {
			this.finish("timeout", statopar);
		}catch(IOException e){}
		soc.close();
	}
	
	private synchronized boolean notQuit(){
		return !quit;
	}
	
	@SuppressWarnings("unchecked")
	private void update(String user, StatoPartita statopar) throws IOException{
		JSONObject notifica = statopar.toJson();
		notifica.put("ACKto", user);
		String objstr = notifica.toJSONString();
		byte[] buf =  encr.encrypt(objstr).getBytes();
		DatagramPacket p = new DatagramPacket(buf, buf.length, group, guessport);
		soc.send(p);  		
	} 
	
	@SuppressWarnings("unchecked")
	private void finish(String f, StatoPartita statopar) throws IOException{
		JSONObject notifica = statopar.toJson();
		notifica.put("fine", f);
		String objstr = notifica.toJSONString();
		byte[] buf = encr.encrypt(objstr).getBytes();
		DatagramPacket p = new DatagramPacket(buf, buf.length, group, guessport);
		soc.send(p);
	}
}

class TimeOutTH extends Thread{
	private int timeoutms;
	private MasterMulticastHandler mmh;
	
	public TimeOutTH(MasterMulticastHandler mmh, int timeoutms){
		this.mmh = mmh;
		this.timeoutms = timeoutms;
	}
	public void run(){
		
		try {
			sleep(timeoutms);
			mmh.timeout();
		} catch (InterruptedException e) {}
		
	}
	
}

