package client;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Iterator;
import java.util.List;

import org.json.simple.JSONObject;

public class ClientRMI extends UnicastRemoteObject implements ClientIF{

	private static final long serialVersionUID = 1L;

	public ClientRMI() throws RemoteException {
		super();
	}

	@Override
	public void notifica(JSONObject partitejson) throws RemoteException {
		int disponibili = new Integer((String) partitejson.get("disponibili").toString());
		int massimo = new Integer((String) partitejson.get("massimo").toString());
		@SuppressWarnings("unchecked")
		List<JSONObject> partitelist = (List<JSONObject>) partitejson.get("partite");
		
		System.out.println();
		System.out.println(String.format("Partite disponibili: %d su %d%n" +
				"Partite in attesa: "+
				"%s%n"
				, disponibili, massimo, partiteformat(partitelist)) );
		System.out.println("scegli: master, guesser o quit");
	}

	private String partiteformat(List<JSONObject> partite){
		String ret = new String();
		Iterator<JSONObject> iter = partite.iterator();
		JSONObject next;
		String master;
		int guesTot;
		int guesMax;
		while(iter.hasNext()){
			next = iter.next();
			master = (String) next.get("master");
			guesTot = new Integer(next.get("guesserTot").toString());
			guesMax = new Integer(next.get("guesserMax").toString());
			ret = ret + String.format(" %n master:%s , giocatori:%d/%d", master, guesTot, guesMax);
		}
		return ret;
	}
}
