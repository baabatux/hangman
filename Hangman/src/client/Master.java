package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.UnknownHostException;
import java.util.concurrent.atomic.AtomicBoolean;


import server.Gruppo;


public class Master {
	
	private MulticastSocket soc;
	private InetAddress gruppo;
	private InetAddress localif;
	private int portaguess;
	private int localport;
	private String chiave;
	private int timeout;
	
	private StatoPartita statopar;

	public Master(Gruppo grup, InetAddress localif, String parola, int errMax, int timeout, String chiave){
		this.statopar = new StatoPartita(parola, errMax);
		this.portaguess = grup.getGuesserport();
		this.localport = grup.getMasterport();
		this.gruppo = grup.getAddress();
		this.localif = localif;
		this.timeout = timeout;
		this.chiave = chiave;
		
	}

	public boolean startgame() {
		try {
			this.soc = new MulticastSocket(localport);
			soc.setInterface(localif);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		AtomicBoolean gameover = new AtomicBoolean(false);
		MasterMulticastHandler mh = new MasterMulticastHandler(gameover, soc, gruppo, statopar, portaguess, timeout, chiave);
		mh.start();
		
		System.out.println("%nPartita cominciata!");
		String strIn = null;
		boolean retval = false;
		boolean inputOK = false;
		
		while(!inputOK) {
			System.out.println("(digita quit per uscire dalla partita)");
			try {
				strIn =  in.readLine();
			} catch (IOException e) {}
			
			if (strIn.equals("quit")) {
				inputOK = true;
				mh.quit();
				if(gameover.get()) retval = false;
				else retval = true;
			}
		}
		return retval;
	}
}
