package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.concurrent.atomic.AtomicBoolean;

public class QuitThread extends Thread {
private Socket soc;
public AtomicBoolean quitted;

	public QuitThread(Socket soc){
		this.soc = soc;
		this.quitted = new AtomicBoolean(false);
	}
	
	public void run(){
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
			while(!quitted.get()){
				String strIN = in.readLine();
				if (strIN.equals("quit")) {
				quitted.set(true);
				soc.close();
				}
				else if(quitted.get()){ /*si è usciti dalla fase di creazione*/ }
				else System.out.println();
			}
		} catch (IOException e) {
			quitted.set(true);
		}
	}
}
