package client;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;

import org.jasypt.util.text.BasicTextEncryptor;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class GuesserMulticastHandler extends Thread {
	private MulticastSocket soc;
	private InetAddress group;
	private byte[] bufrcv;
	private DatagramPacket rcvp;
	private JSONObject obj;
	private JSONParser pars;
	private GuesserStatus guesStat = null;
	private BasicTextEncryptor encr; 
	private boolean quit = false;
	
	public void run(){
		try{ handleGuesser(guesStat);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public GuesserMulticastHandler(MulticastSocket soc, InetAddress group, GuesserStatus gs, String chiave){
		this.soc = soc;
		this.group = group;
		this.bufrcv = new byte[256];
		this.rcvp = new DatagramPacket(bufrcv, bufrcv.length);
		this.pars = new JSONParser();
		this.guesStat = gs;
		encr = new BasicTextEncryptor();
		encr.setPassword(chiave);
	}
	
	
	private void handleGuesser(GuesserStatus gs) throws IOException{
		
		soc.joinGroup(group);
		try{
		while ( notQuit() && !gs.isGameOver()) {
			soc.receive(rcvp);
			synchronized(gs) {
				try {
					String rcvstr = encr.decrypt(new String(rcvp.getData(), 0, rcvp.getLength()));
					obj = (JSONObject) pars.parse(rcvstr);
				
		    		String ackto = (String) obj.get("ACKto");
	     			if (ackto == null) {
	     				gs.gameOver();
	     				gs.ACKrecieved();//considero la fine partita come un ack all'utente
	     				gs.notify();
	     			}
	     			else if (gs.getUsername().equals(ackto) ){
	     				gs.ACKrecieved();
	     				gs.notify();
					}
				System.out.println(obj.toString());
				} catch (ParseException e) {}
			}
		}
		}
		catch (IOException e){
			if (notQuit()) throw new IOException();//propago l'eccezione perché è davvero errore
			else{
				//il "quit" chiude la socket e fa scattare l'eccezione
				System.out.println("Partita abbandonata.");
			}
		}
	
		
	}

	public synchronized void quit(){
		quit = true;
		guesStat.gameOver();
		soc.close();
	}
	
	private synchronized boolean notQuit(){
		return !quit;
	}

		
		
}