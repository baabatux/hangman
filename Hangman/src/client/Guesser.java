package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;


import org.jasypt.util.text.BasicTextEncryptor;
import org.json.simple.JSONObject;

import server.Gruppo;

public class Guesser {
	private String username;
	private MulticastSocket soc;
	private InetAddress gruppo;
	private InetAddress localif;
	private int portamaster;
	private int portalocale;
	private String chiave;
	private BasicTextEncryptor encr;
	private int ACKTO = 5000;
	
	public Guesser(Gruppo grup, InetAddress localif, String chiave, String user){
		this.portamaster = grup.getMasterport();
		this.portalocale = grup.getGuesserport();
		this.gruppo = grup.getAddress();
		this.localif = localif;
		this.username = user;
		this.chiave = chiave;
		encr = new BasicTextEncryptor();
		encr.setPassword(chiave);
	}
	
	@SuppressWarnings("unchecked")
	public boolean startgame() {
		try {
			this.soc = new MulticastSocket(portalocale);
			soc.setInterface(localif);
		} catch (IOException e) {
			return false;
		}
		
		byte[] bufsnd;
		DatagramPacket s;

		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		GuesserStatus status = new GuesserStatus(username);
		
		
		String objstr;
		JSONObject obj;
		String strIn = null;
		
		GuesserMulticastHandler mh = new GuesserMulticastHandler(soc, gruppo, status, chiave);
		mh.start();
		
		System.out.println("%nPartita cominciata!");
		
		boolean retval = false;
		while (!status.isGameOver()){
			System.out.println("%nDigita una lettera o quit:");
			try {
				strIn =  in.readLine();
			} catch (IOException e) {
				return false;
			}
			
			if (!status.isGameOver() && strIn.length() == 1 ){
				obj = new JSONObject();
				obj.put("lettera", strIn);
				obj.put("user", username);
				objstr = obj.toJSONString();
				bufsnd = encr.encrypt(objstr).getBytes();
				s = new DatagramPacket(bufsnd, bufsnd.length, gruppo, portamaster);
				
				synchronized(status){
					try {
						soc.send(s);
					} catch (IOException e1) {
						return false;
					}
					
					status.waitACK();
					try {
						while(status.isWaitingACK()){
							status.wait(ACKTO);
							if (status.isWaitingACK())
								try { soc.send(s);
								} catch (IOException e) {
									return false;
								}
						}
					} catch (InterruptedException e) {
						//è arrivato l'ack. Continuo
					}
				}
			}
			else if (strIn.equals("quit")){
				if(status.isGameOver()){
					retval = false;
				} else {
					mh.quit(); //setta gameOver
					retval = true;
				}
			}		
			
		}
		return retval;
	}
}
