package client;

import java.io.BufferedReader;
import java.io.Console;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.Socket;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import server.Gruppo;
import server.RegistryIF;

public class UserAgent {
	private static int ERRORIMAX;
	private static String SERVERIP;
	private static InetAddress LOCALIF;
	private static int PORTARMI;
	private static int PORTATCP;
	private static String username;
	private static Console console = null;
	private static BufferedReader sysInput = null;

	
	private static void init(String confpath) throws FileNotFoundException, IOException, ParseException{
		JSONObject confobj = (JSONObject) new JSONParser().parse(new FileReader(confpath));
		ERRORIMAX = new Integer(confobj.get("maxerrori").toString());
		SERVERIP =  (String) confobj.get("serverip");
		PORTARMI = new Integer(confobj.get("rmiport").toString());
		PORTATCP = new Integer(confobj.get("tcpport").toString());
		LOCALIF = InetAddress.getByName((String) confobj.get("localip"));
		console = System.console();
		if(console == null) sysInput = new BufferedReader(new InputStreamReader(System.in));
	}
	
	public static void main(String[] argv) throws IOException {
		if (argv.length == 0 || (!argv[0].equals("registra") && !argv[0].equals("login"))) {
			System.out.println("Manca un argomento: login o registra");
			System.exit(1);
		}		
		try { init("UA.conf");	
		} catch (FileNotFoundException e) {
			System.out.println("File UA.conf non trovato");
			System.exit(1);
		} catch (ParseException e){
			System.out.println("File UA.conf corrotto");
			System.exit(1);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
		
		System.out.printf("Max errori: %d%n", ERRORIMAX);
		System.out.printf("LocalInterface: %s%n",LOCALIF);
		
		System.setProperty("java.rmi.server.hostname", LOCALIF.getHostName());
		ClientIF callback = new ClientRMI();
		
		RegistryIF rmi = null; 
		try {
			rmi = (RegistryIF) Naming.lookup("rmi://"+SERVERIP+":"+PORTARMI+"/hangman");
		} catch (MalformedURLException | RemoteException | NotBoundException e) {
			System.err.println("Impossibile collegarsi al server: " + e);
			System.exit(1);
		}
		
		String pwd;
				
		if (argv[0].equals("registra")){
			username = readLine("username: ");
			pwd = readLine("password: ");

			try {
				if (rmi.register(username, pwd)){
					System.out.printf("Registrazione completata!%n");
					System.exit(0);
				}
				else {
					System.out.printf("Username già in uso%n");
					System.exit(1);
				}
			} catch (RemoteException e) {
				System.err.println("Errore connessione con server" + e);
				System.exit(1);
			}
		}
		else if(argv[0].equals("login")){
					
			String scelta;
			boolean quit = false;
			username = readLine("username: ");
			while(!quit){	
				pwd = readLine("password: ");
				try {
					switch(rmi.login(username, pwd, callback)){
					case 0: 
						quit = true;
						break;
					case -1: 
						System.out.println(" Password errata");
						break;
					case -2: 
						System.out.println(" Username già online");
						username = readLine("username: ");
						break;
					case -3: 
						System.out.println(" Username non esistente");
						username = readLine("username: ");
					}
				} catch (RemoteException e) {
					System.err.println("Errore connessione con server " + e);
					System.exit(1);
				}				 
			}
			
			quit = false;
			while (!quit){
				scelta = readLine("scelta? ");
				
				if (scelta.equals("master")){
					String parola = readLine("scegli parola: ");
					int Nguesser = 0;
					boolean inok = false;
					while (!inok){
						try{ 
							Nguesser = new Integer(readLine("scegli numero guesser: "));
						}catch (NumberFormatException e){ System.out.println("Inserisci un intero > 0");}
						inok = true;
					}
					
					try{
						JSONObject rcvobj = TCPmaster(parola, Nguesser);
						JSONObject gruppo =  (JSONObject) rcvobj.get("gruppo");
						
						
						if (gruppo != null){//la partita è cominciata
							int timeout = new Integer(rcvobj.get("timeout").toString());	
							String chiave = (String) rcvobj.get("chiave");
							Master m = new Master(new Gruppo(gruppo), LOCALIF, parola, ERRORIMAX, timeout, chiave);
							if (m.startgame()) rmi.gameover(username);
							else rmi.quitgame(username, username);
						}
						else {
							String esito = (String) rcvobj.get("esito");
							if (esito != null) System.out.println(esito);
						}
					} catch (IOException e){
						System.out.println("Errore connessione:" + e);
					}
				}
				else if (scelta.equals("guesser")) {
					String mastername = readLine("scegli master: ");
					JSONObject rcvobj = TCPguesser(mastername);
					JSONObject gruppo = (JSONObject) rcvobj.get("gruppo");
					
					if(gruppo != null){
						String chiave = (String) rcvobj.get("chiave");
						Guesser gu = new Guesser(new Gruppo(gruppo),LOCALIF, chiave, username);
						if (gu.startgame()) rmi.quitgame(username, mastername);
					} else {
						 String esito = (String) rcvobj.get("esito");
						 if(esito != null) System.out.println(esito);
						}
				
				} else if (scelta.equals("quit")){
					quit = true;
					try {
						rmi.logout(username);
						System.exit(0);
					} catch (RemoteException e) {
						System.err.println("Errore connessione con il server" + e);
					}
				} else System.out.printf("scegliere: master, guesser o quit%n");
			}
		}	
	}	
	
	@SuppressWarnings("unchecked")
	private static JSONObject TCPguesser(String mastername) throws IOException {
		Socket soc = new Socket(SERVERIP, PORTATCP);
		JSONParser pars = new JSONParser();
		PrintWriter out = new PrintWriter(soc.getOutputStream(), true);
		BufferedReader in = new BufferedReader(new InputStreamReader(soc.getInputStream()));
		
		JSONObject snd = new JSONObject();
		snd.put("name", username);
		snd.put("richiesta", "guesser");
		snd.put("mastername", mastername);
		out.println(snd.toJSONString());
		
		JSONObject retval = new JSONObject();
		retval.put("esito", "Error");
		QuitThread qt = new QuitThread(soc);
		qt.start();
		
		try{
			boolean cond = true;
			while(cond){
			
			JSONObject rcv = (JSONObject) pars.parse(in.readLine());
			String esito = (String) rcv.get("esito");
			
			if (esito.equals("join")) {
					retval = rcv;
					System.out.println(rcv);
			} else	
				if (esito.equals("abort")) {
					//System.out.println("Timeout: partita annullata.Quit per continuare");
					retval.put("esito", "Partita annullata. quit per continuare");
					soc.close();
					cond = false;
				} else 
					if (esito.equals("start")){
						snd.clear();
						snd.put("richiesta", "start");
						out.println(snd.toJSONString());
						soc.close();
						cond = false; 
						retval = rcv;
						System.out.println("Partita iniziata");
					}
					else retval = rcv;
			}
			qt.quitted.set(true);
			return retval;

		} catch (IOException | ParseException | NullPointerException e){
			if(qt.quitted.get()){ 
				JSONObject o = new JSONObject();
				o.put("esito", "Quitted");
				return o;
			}
			else {
				qt.quitted.set(true);
				JSONObject o = new JSONObject();
				o.put("esito", "Error");
				return o;
			}
			
		}
	}

		
		
	@SuppressWarnings("unchecked")
	private static JSONObject TCPmaster(String parola, int Nguesser) throws IOException {
		Socket soc = new Socket(SERVERIP, PORTATCP);
		JSONParser pars = new JSONParser();
		PrintWriter out = new PrintWriter(soc.getOutputStream(), true);
		BufferedReader in = new BufferedReader(new InputStreamReader(soc.getInputStream()));  
		
		JSONObject snd = new JSONObject();
		snd.put("name", username);
		snd.put("richiesta", "master");
		snd.put("Nguesser", Nguesser);
		out.println(snd.toJSONString());
		
		JSONObject retval = new JSONObject();
		retval.put("esito", "Error");
		QuitThread qt = new QuitThread(soc);
		
		try{
			JSONObject rcv = (JSONObject) pars.parse(in.readLine());
			String esito = (String) rcv.get("esito");
			
			if (esito.equals("OK")) {
				System.out.println("Partita creata. Attesa giocatori.");
				
				boolean cond = true;
				
				qt.start();
				
				while(cond){	
					rcv = (JSONObject) pars.parse(in.readLine());
					esito = (String) rcv.get("esito");
					
					if (esito.equals("join")) System.out.println(rcv);
					else	
						if (esito.equals("abort")) {
							//System.out.println("Timeout: partita annullata.");
							soc.close();
							cond = false;
							retval.put("esito", "Timeout: partita annullata");
						} else 
							if (esito.equals("start")){
								snd.clear();
								snd.put("richiesta", "start");
								out.println(snd.toJSONString());
								soc.close();
								cond = false;
								retval = rcv;
								System.out.println("Partita iniziata");
							}
					}
				}
			else if(esito.equals("Impossibile creare partita")) retval = rcv;
			qt.quitted.set(true);
			return retval;
		} catch (IOException | ParseException | NullPointerException e){
			if(qt.quitted.get()) {
				JSONObject o = new JSONObject();
				o.put("esito", "Quitted");
				return o;
				}
			else {
				qt.quitted.set(true);
				JSONObject o = new JSONObject();
				o.put("esito", "Error");
				return o;
			}
		}
	}
	
	private static String readLine(String s) throws IOException{
		if (console == null){
			System.out.println(s);
			return sysInput.readLine();
		} else return console.readLine(s);
	}

}
